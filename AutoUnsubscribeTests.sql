-- AUTO UNSUBSCRIBE TESTING PLAN
-- A subscriber's history for a mailing begins on the initial send of a mailing
-- The formatter creates a subscriber_history_new record.
-- The subscriber_history table is irrellevant in this test - that table seems to deal with SMS message tracking and not Informz emails.
-- TEST SETUP

-- ENTER SUBSCRIBER EMAIL
DECLARE @subscriberEmail VARCHAR(50) = 'iamnew@email.com';

-- ENTER BRAND ID
DECLARE @brandID INT = 41102;

DECLARE @ext_84 INT;

DECLARE @mailingId INT = 0;

DECLARE @mailingInstanceId INT = 222223;

DECLARE @mfqId INT = 0;

DECLARE @subscriberId INT;

DECLARE @subscriberHistoryId BIGINT;

-- Ensure we start out with a brand that has ext 84 enabled, if not then enable 

SELECT
  @ext_84 = extension_id from extension_to_brand where brand_id = @brandID and extension_id = 84;

IF @ext_84 = 84

BEGIN

	SELECT
	  @subscriberId = subscriber_id
	from
	  subscriber
	where
	  subscriber_email = @subscriberEmail and brand_id = @brandID;

	-- Ensure we start out with a "subscribed" subscriber
	SELECT
	  cancelled_id as expect_0
	FROM
	  subscriber
	WHERE
	  subscriber_id = @subscriberID;

	-- Create an all new subscriber_history_new record
	EXEC create_subscriber_history_record @subscriberId,
	@mfqId,
	@mailingInstanceId,
	@mailingId;

	-- Retrieve that record's id
	SELECT
	  @subscriberHistoryId = subscriber_history_id
	from
	  subscriber_history_new
	where
	  mailing_instance_id = @mailingInstanceId;

	-- Process the history id with MARK_REPEAT_BOUNCER , expect email_status_id to remain zero
	EXEC MARK_PERMANENT_BOUNCER @subscriberHistoryId;

	select
	  email_status_id as expect_0
	from
	  subscriber_history_new
	where
	  subscriber_history_id = @subscriberHistoryId;

	-- Simulate the formatter marking this as a hard bounce
	update
	  subscriber_history_new
	set
	  email_status_id = 2
	where
	  subscriber_history_id = @subscriberHistoryId;

	-- Process hard bounce
	EXEC MARK_PERMANENT_BOUNCER @subscriberHistoryId;

	select
	  email_status_id as expect_2
	from
	  subscriber_history_new
	where
	  subscriber_history_id = @subscriberHistoryId;

	-- Let's add two more hard Bounces for this subscriber
	-- 1
	EXEC create_subscriber_history_record @subscriberId,
	@mfqId,
	@mailingInstanceId,
	@mailingId;

	SELECT
	  @subscriberHistoryId = subscriber_history_id
	from
	  subscriber_history_new
	where
	  mailing_instance_id = @mailingInstanceId
	order by
	  1 desc;

	UPDATE
	  subscriber_history_new
	set
	  email_status_id = 2
	where
	  subscriber_history_id = @subscriberHistoryId;

	EXEC MARK_PERMANENT_BOUNCER @subscriberHistoryId;

	-- 2
	EXEC create_subscriber_history_record @subscriberId,
	@mfqId,
	@mailingInstanceId,
	@mailingId;

	SELECT
	  @subscriberHistoryId = subscriber_history_id
	from
	  subscriber_history_new
	where
	  mailing_instance_id = @mailingInstanceId
	order by
	  1 desc;

	UPDATE
	  subscriber_history_new
	set
	  email_status_id = 2
	where
	  subscriber_history_id = @subscriberHistoryId;

	EXEC MARK_PERMANENT_BOUNCER @subscriberHistoryId;

	-- With three consecutive bounces, the subscriber should have been unsubscribed
	SELECT
	  subscriber_class as expect_negative_number,
	  cancelled_id as expect_non_zero,
	  subscriber_cancel_message as expect_ext_84_message,
	  subscriber_cancellation_date as expect_now
	FROM
	  subscriber
	WHERE
	  subscriber_email = @subscriberEmail;

	-- Testing complete. Re-subscribe the subscriber for next round
	update
	  subscriber
	set
	  subscriber_class = 0,
	  cancelled_id = 0,
	  subscriber_cancel_message = NULL,
	  subscriber_cancellation_date = NULL
	WHERE
	  subscriber_email = @subscriberEmail
	  and subscriber_id = @subscriberId;

	-- remove dummy history data
	delete from
	  subscriber_history_new
	where
	  mailing_instance_id = @mailingInstanceId
END

ELSE 

BEGIN
	INSERT INTO 
		extension_to_brand
	VALUES
		(@brandid, 84, 0);

		SELECT
	  @subscriberId = subscriber_id
	from
	  subscriber
	where
	  subscriber_email = @subscriberEmail and brand_id = @brandID;

	-- Ensure we start out with a "subscribed" subscriber
	SELECT
	  cancelled_id as expect_0
	FROM
	  subscriber
	WHERE
	  subscriber_id = @subscriberID;

	-- Create an all new subscriber_history_new record
	EXEC create_subscriber_history_record @subscriberId,
	@mfqId,
	@mailingInstanceId,
	@mailingId;

	-- Retrieve that record's id
	SELECT
	  @subscriberHistoryId = subscriber_history_id
	from
	  subscriber_history_new
	where
	  mailing_instance_id = @mailingInstanceId;

	-- Process the history id with MARK_REPEAT_BOUNCER , expect email_status_id to remain zero
	EXEC MARK_PERMANENT_BOUNCER @subscriberHistoryId;

	select
	  email_status_id as expect_0
	from
	  subscriber_history_new
	where
	  subscriber_history_id = @subscriberHistoryId;

	-- Simulate the formatter marking this as a hard bounce
	update
	  subscriber_history_new
	set
	  email_status_id = 2
	where
	  subscriber_history_id = @subscriberHistoryId;

	-- Process hard bounce
	EXEC MARK_PERMANENT_BOUNCER @subscriberHistoryId;

	select
	  email_status_id as expect_2
	from
	  subscriber_history_new
	where
	  subscriber_history_id = @subscriberHistoryId;

	-- Let's add two more hard Bounces for this subscriber
	-- 1
	EXEC create_subscriber_history_record @subscriberId,
	@mfqId,
	@mailingInstanceId,
	@mailingId;

	SELECT
	  @subscriberHistoryId = subscriber_history_id
	from
	  subscriber_history_new
	where
	  mailing_instance_id = @mailingInstanceId
	order by
	  1 desc;

	UPDATE
	  subscriber_history_new
	set
	  email_status_id = 2
	where
	  subscriber_history_id = @subscriberHistoryId;

	EXEC MARK_PERMANENT_BOUNCER @subscriberHistoryId;

	-- 2
	EXEC create_subscriber_history_record @subscriberId,
	@mfqId,
	@mailingInstanceId,
	@mailingId;

	SELECT
	  @subscriberHistoryId = subscriber_history_id
	from
	  subscriber_history_new
	where
	  mailing_instance_id = @mailingInstanceId
	order by
	  1 desc;

	UPDATE
	  subscriber_history_new
	set
	  email_status_id = 2
	where
	  subscriber_history_id = @subscriberHistoryId;

	EXEC MARK_PERMANENT_BOUNCER @subscriberHistoryId;

	-- With three consecutive bounces, the subscriber should have been unsubscribed
	SELECT
	  subscriber_class as expect_negative_number,
	  cancelled_id as expect_non_zero,
	  subscriber_cancel_message as expect_ext_84_message,
	  subscriber_cancellation_date as expect_now
	FROM
	  subscriber
	WHERE
	  subscriber_email = @subscriberEmail;

	-- Testing complete. Re-subscribe the subscriber for next round
	update
	  subscriber
	set
	  subscriber_class = 0,
	  cancelled_id = 0,
	  subscriber_cancel_message = NULL,
	  subscriber_cancellation_date = NULL
	WHERE
	  subscriber_email = @subscriberEmail
	  and subscriber_id = @subscriberId;

	-- remove dummy history data
	delete from
	  subscriber_history_new
	where
	  mailing_instance_id = @mailingInstanceId
END